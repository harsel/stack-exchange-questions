# stack-exchange-questions

> A Vue.js project

## Запуск

Предпочтительнее использовать yarn

``` bash
# установка зависимостей
yarn
npm install

# сервер и hot reload на localhost:8080
yarn dev
npm run dev

# продакшн-сборка
yarn build
npm run build

# продакшн-сборка и анализ бандла
yarn build --report
npm run build --report
```

## Состав проекта

### Структура

/api - методы для работы с api

/components - базовые компоненты приложения

/utils - функции-хелперы

App.vue - корневой компонент приложения, который хранит состояние

main.js - точка входа в приложение

### Использованные зависимости

- Vue
- axios (для запросов)
- bootstrap-vue (в качестве ui-библиотеки)
- lodash
