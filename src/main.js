import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'axios-progress-bar/dist/nprogress.css';

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import App from './App';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
});
