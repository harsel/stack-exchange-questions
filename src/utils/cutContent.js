export default function (content) {
  const preview = content.replace(/<(?:.|\n)*?>/gm, '').slice(0, 250).trim();
  return `${preview}...`;
}
