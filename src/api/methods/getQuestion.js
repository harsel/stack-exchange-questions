import api from '@/api';

const params = {
  filter: '!14nQpE3CqYeFtw7d9XAhn*xneqLWUrIITV.kmN6*ObO-VH',
};

export default async function (id) {
  const { data: { items: [question] } } = await api.get(`/questions/${id}`, { params });
  return question;
}
