import api from '@/api';

export default async function (params) {
  const { data } = await api.get('/search', {
    params: {
      filter: '!)4k*KGW3UhZyv9e4XamlU_ooJ.Vj',
      ...params,
    },
  });

  return data;
}
