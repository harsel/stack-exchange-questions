import axios from 'axios';
import { loadProgressBar } from 'axios-progress-bar';

const params = {
  site: 'stackoverflow',
};

const instance = axios.create({
  baseURL: 'https://api.stackexchange.com/2.2',
  params,
});

loadProgressBar({
  showSpinner: false,
}, instance);

export default instance;
